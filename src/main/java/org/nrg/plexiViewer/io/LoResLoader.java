//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Created on Oct 28, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.nrg.plexiViewer.io;

/**
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 * @author Mohana
 *
 * A class used to load the Lo-Resolution image. 
 * Refers to the Lo-Res element in the specification XML to look at 
 * the file names. 
 * 
 * Will return the default (8bit) lo-res version of the Lo-Res image unless specfied otherwise.   
 * 
 * @see ImageLoaderI.java
 * @see org.cnl.plexiViewer.base.lite.utils.Options.java
 * @see import org.cnl.plexiViewer.base.lite.xml.*;
 */

import ij.ImagePlus;
import ij.io.FileSaver;
import ij.process.StackProcessor;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Hashtable;

import org.nrg.plexiViewer.Writer.AnalyzeWriter;
import org.nrg.plexiViewer.converter.ConverterUtils;
import org.nrg.plexiViewer.exceptions.InvalidParameterValueException;
import org.nrg.plexiViewer.lite.UserSelection;
import org.nrg.plexiViewer.lite.display.MontageDisplay;
import org.nrg.plexiViewer.lite.io.PlexiImageFile;
import org.nrg.plexiViewer.lite.utils.ImageDetails;
import org.nrg.plexiViewer.lite.utils.LiteFileUtils;
import org.nrg.plexiViewer.lite.xml.Layout;
import org.nrg.plexiViewer.lite.xml.LoRes;
import org.nrg.plexiViewer.lite.xml.MontageView;
import org.nrg.plexiViewer.lite.xml.ViewableItem;
import org.nrg.plexiViewer.manager.PlexiSpecDocReader;
import org.nrg.plexiViewer.manager.PlexiStatusPublisherManager;
import org.nrg.plexiViewer.utils.ArchivePathManager;
import org.nrg.plexiViewer.utils.FileUtils;
import org.nrg.plexiViewer.utils.ImageUtils;
import org.nrg.plexiViewer.utils.PlexiConstants;
import org.nrg.plexiViewer.utils.PlexiPublisher;
import org.nrg.plexiViewer.utils.URIUtils;
import org.nrg.plexiViewer.utils.Transform.ReOrientMakeMontage;
import org.nrg.xft.XFT;

public class LoResLoader implements ImageLoaderI {

	private UserSelection userOptions;
	private MontageView mView;
	private Layout layout;
	private MontageDisplay mDisplay;
	private PlexiImageFile openedImageFile;

	/**
	 * Constructor
	 * @param opt: Options for which the lo-res image is to be loaded
	 */
	public LoResLoader(UserSelection opt) {
		userOptions = opt;
	}
	
	public ImagePlus load()  throws  Exception {
		ImagePlus image = null;
		PlexiImageFile pf =		userOptions.getFile();
				layout = ImageDetails.getLayout(pf.getDimX(), pf.getDimY(), pf.getDimZ(), pf.getVoxelResX(), pf.getVoxelResY(), pf.getVoxelResZ(), pf.getFormat());
				String loresLocation = ArchivePathManager.GetInstance().getLoResLocation(userOptions.getProject(),userOptions.getSessionLabel());
				String loresType = ImageDetails.getDefaultLoresType();
                
				pf.setPath(URIUtils.getURI(loresLocation).toString());
				if (pf.getFormat()!=null && pf.getFormat().equals("IMA")) {
					pf.setName(FileUtils.getLoResFileName(pf.getName()+".4dfp.img",loresType, userOptions.getOrientation()));
					
				}else

				pf.setName(FileUtils.getLoResFileName(pf.getName(),loresType, userOptions.getOrientation()));
	            if (pf.getPath().endsWith("/"))
	                pf.setURIAsString(pf.getPath() + pf.getName());
	            else
	                pf.setURIAsString(pf.getPath() + "/" + pf.getName());
				pf.setCachePath(pf.getCachePath());
	            pf.setXsiType(PlexiConstants.PLEXI_IMAGERESOURCE);
				String format=ImageDetails.getDefaultLoresFormat();
				pf.setFormat(format);

				
				System.out.println("Looking out for the Lo Res file " + pf.getPath()+ File.separator + pf.getName());

				PlexiFileOpener pfo = new PlexiFileOpener(pf.getFormat(),pf); 
				image = pfo.getImagePlus();
                if (mView == null) {
                    mView = new MontageView();
                    mView.setScale(MontageView.SCALE);
                    Hashtable attribs = ImageUtils.getSliceIncrement(pf.getDimZ());
                    mView.addViewInfo(userOptions.getOrientation().toUpperCase(),((Integer)attribs.get("startslice")).intValue(),((Integer)attribs.get("endslice")).intValue(),((Integer)attribs.get("increment")).intValue());
                    mDisplay = new MontageDisplay(mView, userOptions.getOrientation().toUpperCase(), layout);
                }

                
                mDisplay = new MontageDisplay(mView, userOptions.getOrientation().toUpperCase(), layout);
				ReOrientMakeMontage rm = new ReOrientMakeMontage(userOptions,pf.getFormat(),getMontageDisplay());
				image = rm.doPerform(image);
				openedImageFile = pf;
				System.out.println("LoResLoader openedImageFIle " + getPlexiImageFile().toString());
                pf.flush();
                openedImageFile.flush();
                //pf = null;
                //openedImageFile = null;
		return image;
	}

	public PlexiImageFile getPlexiImageFile() {
		return openedImageFile;
	}

	/**
	 * @return
	 */
	public MontageView getMontageView() {
		return mView;
	}

	public Layout getLayout() {
		return layout;
	}
	
	public MontageDisplay getMontageDisplay() {
		return mDisplay;
	}
	
	public static void main(String args[]) {
		UserSelection u = new UserSelection();
		u.setSessionId("000115_92046");
		u.setLoResType("8bit");
		u.setDataType("RAW");
		u.setOrientation("SAGITTAL");
		u.setDisplay("STACK");
		u.setHiResLayerNum(0);
		try {
			XFT.init("C:\\jakarta-tomcat-5.5.4\\webapps\\cnda_xnat",true,true);
			LoResLoader l = new LoResLoader(u);
			ImagePlus img = l.load();
			//img.show();

			img=new plexiViewerImageRelayer(u.getOrientation()).reverseStacks(img);
			StackProcessor sp = new StackProcessor(img.getStack(), img.getProcessor());
			sp.flipHorizontal();
			new FileSaver(img).saveAsRawStack("C:\\Mohana\\Temp\\LoRes\\3491-4_8bit_sag.img");
			new AnalyzeWriter().save(img,"C:\\Mohana\\Temp\\LoRes","3491-4_8bit_sag.img",u.getOrientation()+"F");  

			System.out.println("LoResLoader Done");
		}catch(Exception e ) {
			e.printStackTrace();
		}
	}
	

}
