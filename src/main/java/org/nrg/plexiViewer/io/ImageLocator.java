package org.nrg.plexiViewer.io;

import ij.io.FileInfo;

import java.util.Hashtable;

import org.nrg.plexiViewer.Reader.PlexiImageHeaderReader;
import org.nrg.plexiViewer.converter.ConverterUtils;
import org.nrg.plexiViewer.lite.UserSelection;
import org.nrg.plexiViewer.lite.display.MontageDisplay;
import org.nrg.plexiViewer.lite.io.PlexiImageFile;
import org.nrg.plexiViewer.lite.utils.ImageDetails;
import org.nrg.plexiViewer.lite.utils.LiteFileUtils;
import org.nrg.plexiViewer.lite.xml.MontageView;
import org.nrg.plexiViewer.manager.PlexiStatusPublisherManager;
import org.nrg.plexiViewer.utils.ArchivePathManager;
import org.nrg.plexiViewer.utils.FileUtils;
import org.nrg.plexiViewer.utils.ImageUtils;
import org.nrg.plexiViewer.utils.PlexiConstants;
import org.nrg.plexiViewer.utils.URIUtils;

public class ImageLocator {
 public PlexiImageFile getImage(UserSelection options) throws Exception {
		PlexiImageFile hpf = new PlexiImageFile();
		PlexiImageFile pf = new PlexiImageFile();
		String cloneWithoutFiles = options.toString();
		boolean build = false;
		String format=null;

        String cachePathLocation = ArchivePathManager.GetInstance().getCachePathLocation(options.getProject(), options.getSessionLabel());
		String loresLocation = ArchivePathManager.GetInstance().getLoResLocation(options.getProject(),options.getSessionLabel());
		String loresType = ImageDetails.getDefaultLoresType();
    	if (options.hasFile()) { 
			hpf = options.getFile();
		}else if (options.hasXnatFile()) {
			hpf = IOHelper.getPlexiImageFile(options.getXnatFile(), cachePathLocation, LiteFileUtils.getFileName(options.getXnatFile()));
		}
		System.out.println("ImageLocator HIRes File Details are " + hpf);
		//Try to look for LORES FILE first
		options.setLoResType(loresType);
		System.out.println("ImageLocator LoRes Type to look out for are " + options.getLoResType());
			pf.setPath(URIUtils.getURI(loresLocation).toString());
			if (hpf.getFormat()!=null && hpf.getFormat().equals("IMA")) {
				pf.setName(FileUtils.getLoResFileName(hpf.getName()+".4dfp.img",loresType, options.getOrientation()));
				
			}else
			pf.setName(FileUtils.getLoResFileName(hpf.getName(),loresType, options.getOrientation()));
            if (pf.getPath().endsWith("/"))
                pf.setURIAsString(pf.getPath() + pf.getName());
            else
                pf.setURIAsString(pf.getPath() + "/" + pf.getName());
			pf.setCachePath(hpf.getCachePath());
            pf.setXsiType(PlexiConstants.PLEXI_IMAGERESOURCE);
			format=ImageDetails.getDefaultLoresFormat();
			System.out.println("ImageLocator LoRes File to be looked out for " + pf.toString() + " Format " + format);
		pf =  FileUtils.fileExists(pf);
        if (pf == null) {    
			pf=hpf;                     
            format= pf.getFormat();
            build = true;
        }

            System.out.println("ImageLocator will look for " + pf.toString());
//           	options.setFile(hpf);
            if (build) {
            	PlexiStatusPublisherManager.GetInstance().getPublisher(cloneWithoutFiles).setValue("Building requested Image");
            	pf  = ConverterUtils.convert(options);
    			format = pf.getFormat();
            }else {
            	PlexiStatusPublisherManager.GetInstance().getPublisher(cloneWithoutFiles).setValue("Delivering requested Image");
            }
			PlexiImageHeaderReader xIReader = new PlexiImageHeaderReader(format);//Returns the header type ie ANALYZE or IFH etc
            String path = pf.getPath();
            if (path.startsWith("file:")) path = path.replaceAll("file:","");
			//READ THE LORES FILE DETAILS
            FileInfo fi = xIReader.getFileInfo(path,pf.getName()); //directory and filename
            pf = hpf;
            //ON THE SUBSEQUENT CALL THE HIRES DETAILS ARE AVAILABLE
     /*       pf.setURIAsString(hpf.getURIAsString());
			pf.setFormat(hpf.getFormat());
			if (hpf.getResourceCatalogPath()!=null) pf.setResourceCatalogPath(hpf.getResourceCatalogPath());
			else {
				pf.setPath(hpf.getPath());
				pf.setName(hpf.getName());
			}
            pf.setXsiType(hpf.getXsiType());
            if (hpf.getCachePath() != null)
                pf.setCachePath(hpf.getCachePath()); */
			//pf.setDimX(ImageUtils.getWidth(fi.width, fi.height, fi.nImages, xIReader.getOrientation(), options.getOrientation()));
			//pf.setDimY(ImageUtils.getHeight(fi.width, fi.height, fi.nImages, xIReader.getOrientation(), options.getOrientation()));
			//pf.setDimZ(ImageUtils.getStackSize(fi.width, fi.height, fi.nImages, xIReader.getOrientation(), options.getOrientation()));
            //SEND DIMENSIONS OF THE LORES FILE WHICH IS TO BE DISPLAYED
            pf.setDimX(fi.width);
            pf.setDimY(fi.height);
            pf.setDimZ(fi.nImages);
            pf.setFileType(fi.fileType);
            pf.setVoxelResX(fi.pixelWidth);
            pf.setVoxelResY(fi.pixelHeight);
            pf.setVoxelResZ(fi.pixelDepth);
			
			
			return pf;
 }
}
