//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Created on Oct 28, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.nrg.plexiViewer.io;

/**
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 * @author Mohana
 *
 * Interface for ImageLoaders
 * @see HiResLoader, LoResLoader, ThumbnailLoader
 */
import ij.ImagePlus;

import java.net.URISyntaxException;

import org.nrg.plexiViewer.exceptions.InvalidParameterValueException;
import org.nrg.plexiViewer.lite.display.MontageDisplay;
import org.nrg.plexiViewer.lite.io.PlexiImageFile;


public interface ImageLoaderI {
	public ImagePlus load()  throws   Exception ;
	public MontageDisplay getMontageDisplay();
	public PlexiImageFile getPlexiImageFile();
	
}
