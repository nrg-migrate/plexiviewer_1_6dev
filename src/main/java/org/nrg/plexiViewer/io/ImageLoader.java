//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.nrg.plexiViewer.io;

/**
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 * @author Mohana
 * 
 * @see org.cnl.plexiViewer.base.lite.utils.Options.java
 *	
 * This class will fetch the image by referring to the Options passed
 */

import ij.*;
import ij.process.*;
import org.nrg.plexiViewer.lite.*;
import org.nrg.plexiViewer.lite.io.*;
import org.nrg.plexiViewer.lite.display.*;
import org.nrg.xft.*;
import org.nrg.plexiViewer.lite.xml.*;
import org.nrg.plexiViewer.manager.*;

public class ImageLoader {

	UserSelection opt;	
	PlexiImageFile openedImageFile;
	/**
	 * mDisplay is populated with the MontageDisplay information as in the Specification XML
	 * for the LoRes/HiRes image type.
	 */
	MontageDisplay mDisplay;
	
	
	/**
	 * Constructor 
	 * @param options: the options for which the image is to be fetched
	 * @see org.cnl.plexiViewer.base.lite.utils.Options.java
	 */
	public ImageLoader(UserSelection options) {
		opt = options;
	}
	
	/**
	 * Returns the associated image based on the imageType. 
	 * Populates the  montageDisplay from the XML for the image type.
	 * 
	 * @param imageType: Specify LoRes, Thumbnail, HiRes
	 * @param convertHiResToDefaultLoRes: boolean 
	 * @return associated Image as an ImagePlus object
	 */
	public ImagePlus getImage(String imageType, boolean convertHiResToDefaultLoRes)  {
		ImagePlus img = null;
		try {
			ImageLoaderI loader= null;
			//if (imageType.equalsIgnoreCase("Thumbnail"))
			//	loader = new ThumbnailLoader(opt);
			if (imageType.equalsIgnoreCase("LoRes"))
				loader = new LoResLoader(opt);	
			else if (imageType.equalsIgnoreCase("HiRes"))
				loader = new HiResLoader(opt);
			img = loader.load();
			System.out.println("ImageLoader is in charge");
			if (img!=null) {
				mDisplay = loader.getMontageDisplay();
				openedImageFile = loader.getPlexiImageFile();
				System.out.println("ImageLoader openedIamgeFile " + openedImageFile.toString());
				System.out.println("ImageLoader getIamgeFile " + getPlexiImageFile().toString());
			}
					
		}catch(Exception e) {
			e.printStackTrace();
		}
		return img;		
	}

	   // returns true if this is an imported signed 16-bit image
		boolean isSigned16Bit(ImagePlus imp)  {
			if (imp.getType()!=ImagePlus.GRAY16)
			   return false;
		   else
			   return imp.getCalibration().getCValue(0)==-32768;
		}

	   // adds the specified value to every pixel in the stack
	   void add(ImagePlus imp, int value) {
		   //IJ.log("add: "+value);
		   ImageStack stack = imp.getStack();
		   for (int slice=1; slice<=stack.getSize(); slice++) {
			   ImageProcessor ip = stack.getProcessor(slice);
			   short[] pixels = (short[])ip.getPixels();
			   for (int i=0; i<pixels.length; i++)
				   pixels[i] = (short)((pixels[i]&0xffff)+value);
		   }
		}


	public ImagePlus getImage()  {
		ImagePlus img = null;
		try {
			ImageLoaderI loader= null;
			try {
				loader = new LoResLoader(opt);
			}catch(Exception e) {
				loader = new HiResLoader(opt);
			}
			img = loader.load();
			System.out.println("ImageLoader is in charge");
			openedImageFile = loader.getPlexiImageFile();
			//System.out.println("ImageLoader openedIamgeFile " + openedImageFile.toString());
			//System.out.println("ImageLoader getIamgeFile " + getPlexiImageFile().toString());

			if (img!=null)
				mDisplay = loader.getMontageDisplay();	
		}catch(Exception e) {
			e.printStackTrace();
		}
		return img;		
	}

	public PlexiImageFile getPlexiImageFile() {
		return openedImageFile;
	}
		
	public MontageDisplay getMontageDisplay() {
		return mDisplay; 
	}

	public static void main(String args[]) {
		UserSelection u = new UserSelection();
		u.setSessionId("000112_vc1552");
		u.setDataType("T88");
		u.setOrientation("Coronal");
		u.setDisplay("Montage");
		u.setHiResLayerNum(0);
		try {
			XFT.init("C:\\jakarta-tomcat-5.5.4\\webapps\\cnda_xnat",true,true);
			ImageLoader loader = new ImageLoader(u);
			ImagePlus image = loader.getImage();
			image.show();
            System.out.println("PLexi Image File " + loader.getPlexiImageFile().toString());
			System.out.println("Done ");
		}catch(Exception e ) {
			e.printStackTrace();
		}
	}

}
