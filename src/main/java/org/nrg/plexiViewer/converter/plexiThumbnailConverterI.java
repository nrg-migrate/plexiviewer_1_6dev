//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.nrg.plexiViewer.converter;

/**
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 * 
 * @author Mohana
 *
 */

import org.nrg.plexiViewer.lite.*;
public interface plexiThumbnailConverterI {
	public int convertAndSave(UserSelection options);	
}