//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.nrg.plexiViewer.converter;

/**
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 * 
 * @author Mohana
 *
 */


import java.net.URISyntaxException;

import org.nrg.plexiViewer.lite.UserSelection;
import org.nrg.plexiViewer.lite.io.PlexiImageFile;

public interface plexiLoResConverterI {
	public int convertAndSave(UserSelection options);	
	public PlexiImageFile getFileLocationAndName() throws URISyntaxException, Exception;
}
