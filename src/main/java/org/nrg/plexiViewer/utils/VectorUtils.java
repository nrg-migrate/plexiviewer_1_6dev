//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright Washington University, St Louis, 2005
 * 
 * Created on Jul 26, 2005
 *
 * 
 */
package org.nrg.plexiViewer.utils;

/**
 * @author Mohana
 *
 */
import java.util.*;
public class VectorUtils {
	
	public static Vector intersect(Vector v1, Vector v2) {
		Vector rtn = new Vector();
		for (int i = 0; i < v1.size(); i++) {
			if (v2.contains(v1.elementAt(i))) {
				rtn.add(v1.get(i));	
			}	
		}
		return rtn;
	}
	
}
