package org.nrg.plexiViewer.utils;

import javax.servlet.http.HttpServletRequest;

import org.nrg.xdat.security.XDATUser;

public class XnatLoginDetails {
	public XDATUser getSessionUser(HttpServletRequest httpRequest) {
		return (XDATUser) httpRequest.getSession().getAttribute("user");
	}

}
