//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright Washington University, St Louis, 2005
 * 
 * Created on Nov 21, 2005
 *
 * 
 */
package org.nrg.plexiViewer.utils;

/**
 * @author Mohana
 *
 */
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.nrg.xft.XFT;
public class MailUtils {
		
	public static void send(String subject,String body)
	  {
		try
		{
		  Properties props = System.getProperties();

		  // -- Attaching to default Session, or we could start a new one --

		  props.put("mail.smtp.host", "artsci.wustl.edu");
		  javax.mail.Session session = javax.mail.Session.getDefaultInstance(props, null);

		  // -- Create a new message --
		  Message msg = new MimeMessage(session);

		  // -- Set the FROM and TO fields --
		  msg.setFrom(new InternetAddress(XFT.GetAdminEmail()));
		  msg.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(XFT.GetAdminEmail(), false));

		  // -- We could include CC recipients too --
		  // if (cc != null)
		  // msg.setRecipients(Message.RecipientType.CC
		  // ,InternetAddress.parse(cc, false));

		  // -- Set the subject and body text --
		  msg.setSubject(subject);
		  msg.setText(body);

		  // -- Set some other header information --
		  msg.setHeader("X-Mailer", "LOTONtechEmail");
		  msg.setSentDate(new Date());

		  // -- Send the message --
		  Transport.send(msg);

		  System.out.println("Message sent OK.");
		}
		catch (Exception ex)
		{
		  ex.printStackTrace();
		}
	  }
}


