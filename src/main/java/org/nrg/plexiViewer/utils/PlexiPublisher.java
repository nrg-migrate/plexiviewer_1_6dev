//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.nrg.plexiViewer.utils;
import java.util.Observable;
/*
 * 
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 * @author Mohana
 */

public class PlexiPublisher extends Observable {
	private Object msg;
	
	public PlexiPublisher() {
		super();
	}
	
	public void setValue(Object message)
	   {
		  this.msg = message;
		  setChanged();
		  notifyObservers();
	   }

	   public Object getValue()
	   {
		  return msg;
	   }	
		
}
