package org.nrg.plexiViewer.lite.utils;

import org.nrg.xdat.bean.XnatAbstractresourceBean;
import org.nrg.xdat.bean.XnatResourceBean;

public class LiteFileUtils {
	public static String getFileName(XnatAbstractresourceBean file){
		String rtn = "";
		if (file instanceof XnatResourceBean) {
			XnatResourceBean resource = (XnatResourceBean)file;
			String uri = resource.getUri();
			int i = uri.lastIndexOf("/");
			if (i != -1) {
				rtn = uri.substring(i+1); //assuming doesnt end in /
			}
		}
		return rtn;
	}

	public static String getFilePath(XnatAbstractresourceBean file){
		String rtn = "";
		if (file instanceof XnatResourceBean) {
			XnatResourceBean resource = (XnatResourceBean)file;
			String uri = resource.getUri();
			int i = uri.lastIndexOf("/");
			if (i != -1) {
				rtn = uri.substring(0,i); //assuming doesnt end in /
			}
		}
		return rtn;
	}

}
