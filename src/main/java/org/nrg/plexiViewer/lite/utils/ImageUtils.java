//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright Washington University, St Louis, 2005
 * 
 * Created on Nov 2, 2005
 *
 * 
 */
package org.nrg.plexiViewer.lite.utils;

/**
 * @author Mohana
 *
 */
import ij.io.FileInfo;
public class ImageUtils {

	public static String initImagePlus(int type) {
		String rtn=null;
		if (type == FileInfo.GRAY8 || type== FileInfo.COLOR8|| type==FileInfo.BITMAP)
			rtn= "ij.process.ByteProcessor";
		else if (type == FileInfo.RGB||type== FileInfo.BGR || type==FileInfo.ARGB || type==FileInfo.RGB_PLANAR) 
			rtn="ij.process.ColorProcessor";
		else if (type == FileInfo.GRAY16_SIGNED || type == FileInfo.GRAY16_UNSIGNED || type==FileInfo.GRAY12_UNSIGNED) 
			rtn="ij.process.ShortProcessor";
		else
			rtn="ij.process.FloatProcessor";
		return rtn;	
	}
}
