//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright Washington University, St Louis, 2005
 * 
 * Created on Sep 19, 2005
 *
 * 
 */
package org.nrg.plexiViewer.lite.plugIns;

/**
 * @author Mohana
 *
 */
import org.nrg.plexiViewer.lite.ui.GenericIdentifier;
public class FileNamingPlugin {
	public GenericIdentifier getFileNameToDisplay(String exptType, String fileName) {
		GenericIdentifier rtn= new GenericIdentifier(fileName,fileName,true);
		if (exptType.equalsIgnoreCase("Gleek")) {
			int indexOfUnderScore = fileName.lastIndexOf("_");
			int secondLastIndexOfUnderScore = fileName.substring(0,indexOfUnderScore).lastIndexOf("_");
			String id;
			if (secondLastIndexOfUnderScore>0) {
				 id=fileName.substring(secondLastIndexOfUnderScore+1);
			}else
				id = fileName.substring(indexOfUnderScore);
			rtn.setId(id);	
		} 
		return rtn;
	}
	
	
}
