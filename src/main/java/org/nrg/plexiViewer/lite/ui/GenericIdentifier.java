//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright Washington University, St Louis, 2005
 * 
 * Created on Jul 27, 2005
 *
 * 
 */
package org.nrg.plexiViewer.lite.ui;

/**
 * @author Mohana
 *
 */
public class GenericIdentifier implements java.io.Serializable {
	Object id;
	Object value;
	boolean sendId=false;
	
	public GenericIdentifier(Object id, Object value) {
		this.id = id;
		this.value= value;
	}
	
	public GenericIdentifier() {
	}

	public GenericIdentifier(Object id, Object value, boolean sendid) {
		this.id = id;
		this.value= value;
		this.sendId = sendid;
	}
	
	
	/**
	 * @return
	 */
	public Object getId() {
		return id;
	}

	/**
	 * @return
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param object
	 */
	public void setId(Object object) {
		id = object;
	}

	/**
	 * @param object
	 */
	public void setValue(Object object) {
		value = object;
	}
	
	public String toString() {
		String rtn = value.toString();
		if (sendId)
			rtn = id.toString();
		return rtn;	
	}

}
