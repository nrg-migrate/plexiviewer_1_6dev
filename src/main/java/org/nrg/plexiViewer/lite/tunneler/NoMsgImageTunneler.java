//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.nrg.plexiViewer.lite.tunneler;

import java.net.*;
import java.io.*;
import ij.*;
import ij.gui.*;
import org.nrg.plexiViewer.lite.manager.*;
import org.nrg.plexiViewer.lite.utils.HTTPDetails;
import org.nrg.plexiViewer.lite.utils.LoadErrorDialog;
import org.nrg.plexiViewer.lite.ui.*;
import org.nrg.plexiViewer.lite.display.*;
import org.nrg.plexiViewer.lite.io.*;
import org.nrg.plexiViewer.lite.*;


/** When this class is built, it returns a value
 *  immediately, but this value returns false for isDone
 *  and null for getQueries. Meanwhile, it starts a Thread
 *  to request an array of query strings from the server,
 *  reading them in one fell swoop by means of an
 *  ObjectInputStream. Once they've all arrived, they
 *  are placed in the location getQueries returns,
 *  and the isDone flag is switched to true.
 *  Used by the ShowQueries applet.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://www.coreservlets.com/.
 *  &copy; 2000 Marty Hall; may be freely used or adapted.
 **/

public class NoMsgImageTunneler implements Runnable {
    private URL dataURL;
    private String host;
    private URLConnection servletConnection;
	private boolean windowShowing;    
	Thread queryRetriever;
	PlexiImageViewerI  image;
	UserSelection userSelection;
	int index;
		
	public NoMsgImageTunneler(  PlexiImageViewerI  i) {
		image=i;
		this.host = HTTPDetails.getHost();
		windowShowing=false;
		userSelection=null;
	}
    
    private void openConnection() {
		String suffix = HTTPDetails.getSuffix("ImageDistributorServlet");
		try {
			dataURL = new URL(HTTPDetails.getProtocol(), HTTPDetails.getHost(), HTTPDetails.getPort(), suffix);
			servletConnection = dataURL.openConnection();
			servletConnection.setDoInput(true);          
			servletConnection.setDoOutput(true);
			//Don't use a cached version of URL connection.
			servletConnection.setUseCaches (false);
			servletConnection.setDefaultUseCaches (false);
			//Specify the content type that we will send binary data
			servletConnection.setRequestProperty ("Content-Type", "application/octet-stream");
			ObjectOutputStream outStreamToServlet = new ObjectOutputStream(servletConnection.getOutputStream());
			outStreamToServlet.writeObject(userSelection);	
			outStreamToServlet.flush();
			outStreamToServlet.close();
		} catch(MalformedURLException mfe) {
			mfe.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
    }
    

	public void load(UserSelection u, int i) {
		index =i;
		userSelection = u;
		queryRetriever = new Thread(this,"NoMsgImageTunneler");
		queryRetriever.start();
	}

    
    public void run() {
       	try {
   	        retrieveImage();
       } catch(IOException ioe) {
       	    ioe.printStackTrace();
       	}catch (OutOfMemoryError ome) {
			image.handleOutOfMemoryError();
   		}
    }
    
	private void retrieveImage() throws IOException {
	   openConnection();
	   InputStream is = servletConnection.getInputStream();
	   ObjectInputStream in =  new ObjectInputStream(is);
	   try {
		   LoadStatus status = (LoadStatus)in.readObject();
		   if (!status.isSuccess()){
			   PlexiImageFile pf = new PlexiImageFile();
			   pf.setDimX(200); pf.setDimY(200); pf.setDimZ(0);
			   //LoadErrorDialog led = new LoadErrorDialog(image.getImagePlus().getWindow(), status.getMessage(), false);
			   //led.show(); 
			   //image.getImagePlus().getWindow().close();
			   return;
		   }
						
		   int nSlices = status.getCount();
		   int width = status.getDimensions().width;
		   int height = status.getDimensions().height;
		   Object pixels;
		   ImageStack stack = new ImageStack(width,height);
		   MontageDisplay display = (MontageDisplay)in.readObject();

		   for (int i =0; i<nSlices; i++){
			   pixels = (Object)in.readObject();
			   stack.addSlice("",pixels);
		   }
		   image.setImage(index,stack); 
	   } catch(ClassNotFoundException cnfe) {
		   cnfe.printStackTrace();
	   } catch (OutOfMemoryError ome) {
	   		image.handleOutOfMemoryError();
	   }
	   in.close();
	   is.close();
   }

}
