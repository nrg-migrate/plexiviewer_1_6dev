//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright Washington University, St Louis, 2005
 * 
 * Created on May 24, 2005
 *
 * This class represents the SchemaLink element of the PlexiViewer Schema. 
 * The entry in the element will be matched against the value attribute in
 * order to fetch the required details.
 */
package org.nrg.plexiViewer.lite.xml;
/**
 * @author Mohana
 *
 */
public class SchemaLink implements Cloneable {
	String element;
	String value=null;
	
	/**
	 * @return
	 */
	public String getElementName() {
		return element;
	}

	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param string
	 */
	public void setElementName(String string) {
		element = string;
	}

	/**
	 * @param string
	 */
	public void setValue(String string) {
		value = string;
	}
	
	public boolean hasValue() {
		boolean rtn=false;
		if (value!=null) 
			rtn=true;
		return rtn;	
	}
	
	public String toString() {
		String rtn = "Element Name " + element + "\n";
		rtn +="Value  " + value ;
		return rtn;
	}
    
    public Object clone() {
        try {
            return super.clone();
        }catch(CloneNotSupportedException e) {
            return null;
        }
}

}
