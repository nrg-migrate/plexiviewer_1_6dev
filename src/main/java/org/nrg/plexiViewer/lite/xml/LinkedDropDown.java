//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright Washington University, St Louis, 2005
 * 
 * Created on May 19, 2005
 *
 * 
 */
package org.nrg.plexiViewer.lite.xml;
/**
 * @author Mohana
 * This class represents the LinkedDropDown element of the specification document
 *
 */
public class LinkedDropDown implements Cloneable{
	String viewableItemType;

	/**
	 * @return
	 */
	public String getViewableItemType() {
		return viewableItemType;
	}


	public String toString() {
		return viewableItemType;
	}
	
	/**
	 * @param string
	 */
	public void setViewableItemType(String string) {
		viewableItemType = string;
	}
    
    public Object clone() {
        try {
            return super.clone();
        }catch(CloneNotSupportedException e) {
            return null;
        }
}

}
