//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright Washington University, St Louis, 2005
 * 
 * Created on May 19, 2005
 *
 * 
 */
package org.nrg.plexiViewer.lite.xml;

/**
 * @author Mohana
 * This class represents the Thumbnail element of the Viewer Spec File
 */
import java.util.*;

import org.nrg.plexiViewer.lite.io.PlexiImageFile;

public class Thumbnail implements Cloneable {
	CropDetails cropDetails;
	String converterClassName;
	Hashtable slices;
	String format;
	
	
	public Thumbnail() {
		slices = new Hashtable();
	}
	
	/**
	 * @return
	 */
	public CropDetails getCropDetails() {
		return cropDetails;
	}

	
	public Vector getSlices(String orientation) {
		return (Vector)slices.get(orientation);
	}

	/**
	 * @return
	 */
	public Hashtable getSlices() {
		return slices;
	}

	/**
	 * @param details
	 */
	public void setCropDetails(CropDetails details) {
		cropDetails = details;
	}

	/**
	 * @param hashtable
	 */
	public void setSlices(Hashtable hashtable) {
		slices = hashtable;
	}
	
	public void setSlice(String orientation, Vector sliceNumbers) {
		slices.put(orientation,sliceNumbers);
	}
	/**
	 * @return
	 */
	public String getConverterClassName() {
		return converterClassName;
	}

	/**
	 * @param string
	 */
	public void setConverterClassName(String string) {
		converterClassName = string;
	}

	/**
	 * @return
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @param string
	 */
	public void setFormat(String string) {
		format = string;
	}

    public Object clone() {
        try {
            Thumbnail deepClone = (Thumbnail)super.clone();
            if (slices==null) {
                deepClone.setSlices(null);                    
            }else {
                Enumeration enumerator = slices.keys();
                while (enumerator.hasMoreElements()) {
                    String key = (String)enumerator.nextElement();
                    Vector value = (Vector)slices.get(key);
                    Vector cloneValue = new Vector();
                    for (int i=0;i<value.size();i++) {
                        cloneValue.addElement((Integer)value.elementAt(i));     
                    }
                    deepClone.setSlice(key,cloneValue);
                }
            }
            return deepClone;
        }catch(CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

}
