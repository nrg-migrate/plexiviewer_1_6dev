//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.nrg.plexiViewer.lite.viewer.MR;
/*
 * MRWindow.java
 *
 * Created on September 24, 2001, 9:46 AM
 */
 

/**
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 * @author  danm
 * @version 
 */
public interface MRWindow {

    public int getCurrentTool();
    public void setMessage(String d, String m);
    public void message();
    
}

