//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.nrg.plexiViewer.lite.manager;

/**
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 * 
 * @author Mohana
 * An interface for all the Image viewers. 
 */
import ij.*;
import org.nrg.plexiViewer.lite.xml.*;
import org.nrg.plexiViewer.lite.display.*;
import org.nrg.plexiViewer.lite.*;
import org.nrg.plexiViewer.lite.gui.*;
import org.nrg.plexiViewer.lite.io.*;
import java.awt.Rectangle;

public interface PlexiImageViewerI {

    
	public int open(PlexiMessagePanel msgPanel);
	public void handleOutOfMemoryError();
	public void setWindowDimensions(PlexiImageFile pf);
	public void setWindowDimensions(int w, int h, int s);
	public UserSelection getUserSelection();
	public ImagePlus getImagePlus();
	public Layout getLayout();
	public void adjustCoords(int x, int y, int z, float ipvalue, boolean slice, String fromView, boolean fromRadiologic);
	public void adjustRegions(int x, int y, int z, float ipvalue, boolean slice, String fromView, boolean fromRadiologic, int region);
	public void setWaitCursor(boolean status);
	public void setMessage(String msg, String desc);
	public void setMontageDisplay(MontageDisplay display);	
	public Rectangle getWindowDimensions();
	public boolean isInitialized();
	public void setImage(int index, ImageStack s);
	public void setImage(int index, ImagePlus s);
	public void updateCrossHair();
	public void updateCrossHair(String orientation, int sliceNo);
}
