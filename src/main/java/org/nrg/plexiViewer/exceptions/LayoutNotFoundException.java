//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.nrg.plexiViewer.exceptions;

/**
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 * 
 * @author Mohana
 *
 */
public class LayoutNotFoundException extends Exception {
	public LayoutNotFoundException() {
	}

	public LayoutNotFoundException(String msg) {
		super(msg);
	}
}
