//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.nrg.plexiViewer.exceptions;

/**
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 * 
 * @author Mohana
 *
 */
public class InsufficientArgumentsException extends Exception {
		public InsufficientArgumentsException() {
		}

		public InsufficientArgumentsException(String msg) {
			super(msg);
		}
}
