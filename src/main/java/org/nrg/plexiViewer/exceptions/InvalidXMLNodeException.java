//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright Washington University, St Louis, 2005
 * 
 * Created on Jul 13, 2005
 *
 * 
 */
package org.nrg.plexiViewer.exceptions;

/**
 * @author Mohana
 *
 */
public class InvalidXMLNodeException extends Exception {

public InvalidXMLNodeException() {
}

public InvalidXMLNodeException(String msg) {
	super("Dont know how to deal with element :: " + msg);
}

}
