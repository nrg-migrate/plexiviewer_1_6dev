//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright Washington University, St Louis, 2005
 * 
 * Created on Jul 26, 2005
 *
 * 
 */
package org.nrg.plexiViewer.Servlet;

/**
 * @author Mohana
 *
 */
import java.io.ObjectOutputStream;
import java.io.ObjectStreamConstants;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nrg.plexiViewer.utils.Session;
import org.nrg.xdat.bean.XnatImagesessiondataBean;

public class PopulateServlet  extends HttpServlet  {
	
		/** Initializes the servlet.
	   */
	  public void init(ServletConfig config) throws ServletException {
		  super.init(config);
	  }
    
	  /** Destroys the servlet.
	   */
	  public void destroy() {
        
	  }
	  
	/** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
	  * @param request servlet request
	  * @param response servlet response
	  */
	 protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, java.io.IOException {
		System.out.println("Recd the post\n");
	 }
		 
		 
	/** Handles the HTTP <code>POST</code> method.
		   * @param request servlet request
		   * @param response servlet response
		   */
	  protected void doPost(HttpServletRequest request, HttpServletResponse response)
	  throws ServletException, java.io.IOException {
		  processRequest(request, response);
	  }
	
	/** Handles the HTTP <code>GET</code> method.
			 * @param request servlet request
			 * @param response servlet response
			 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, java.io.IOException {
		String contentType = "application/x-java-serialized-object";
		response.setContentType(contentType);
		String sessionId = request.getParameter("sessionId");
		//Integer exptId = new Integer(Integer.parseInt(request.getParameter("exptId")));
		Session mrSession = new Session(sessionId);
		XnatImagesessiondataBean rtn=mrSession.get();
		ObjectOutputStream outToApplet = new ObjectOutputStream(response.getOutputStream());
		outToApplet.useProtocolVersion(ObjectStreamConstants.PROTOCOL_VERSION_1);
		outToApplet.writeObject(rtn);
		outToApplet.flush();
		outToApplet.close();
	}

}
