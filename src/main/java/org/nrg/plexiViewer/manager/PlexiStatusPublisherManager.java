//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.nrg.plexiViewer.manager;

/**
 * COPYRIGHT WASHINGTON UNIVERSITY 2005
 *  
 * @author Mohana
 *
 *	A class to hold the Publishers for a given Option. All classes that need to update the status 
 *  on the client side, will invoke get the publisher from this Manager. 
 * 
 * The Publisher term here refers to the Publisher/Subcriber Pattern ie Observable/Observer Pattern.
 */
import java.util.*;

import org.nrg.plexiViewer.lite.UserSelection;
import org.nrg.plexiViewer.utils.PlexiPublisher;
import org.nrg.plexiViewer.utils.PlexiSubscriberProxy;
public class PlexiStatusPublisherManager {
	
	private Hashtable optionsPublisherHash = null;
	private Hashtable optionsSubscriberHash = null;
	private Hashtable optionsHandledHash = null;
	private static PlexiStatusPublisherManager self = null;
	private PlexiStatusPublisherManager() {
		optionsPublisherHash = new Hashtable();
		optionsSubscriberHash = new Hashtable();
		optionsHandledHash = new Hashtable();
	}
	
	public static PlexiStatusPublisherManager GetInstance() {
		if (self==null)
			self = new PlexiStatusPublisherManager();
		return self;		
	}
	
	public  void createPublisher(String opt) {
		if (optionsPublisherHash==null || optionsPublisherHash.size()==0) {
			optionsPublisherHash.put(opt,new PlexiPublisher());	
		}else if (!optionsPublisherHash.containsKey(opt)) {
				optionsPublisherHash.put(opt,new PlexiPublisher());
		}
	}

	public  void createSubscriber(String opt) {
		if (optionsSubscriberHash==null || optionsSubscriberHash.size()==0) {
			optionsSubscriberHash.put(opt,new PlexiSubscriberProxy(opt));	
		}else if (!optionsSubscriberHash.containsKey(opt)) {
			optionsSubscriberHash.put(opt,new PlexiSubscriberProxy(opt));
		}
		optionsHandledHash.put(opt,new Boolean(false));
	}

		
	public  PlexiPublisher getPublisher(String options) {
		//createPublisher(options);
		return (PlexiPublisher)	optionsPublisherHash.get(options);
	}
	
	public void removePublisher(String opt) {
		if (optionsPublisherHash.containsKey(opt)) 
			optionsPublisherHash.remove(opt);
	}
	
	public  PlexiSubscriberProxy getSubscriberProxy(String opt) {
		PlexiSubscriberProxy rtn = (PlexiSubscriberProxy)optionsSubscriberHash.get(opt);
		return rtn;
	}
	
	public  void setHandled(String opt) {
		if (optionsHandledHash.containsKey(opt))
			optionsHandledHash.remove(opt);
		optionsHandledHash.put(opt,new Boolean(true)); 
	}
	
	public  Object isHandled(String opt) {
		return optionsHandledHash.get(opt);
	}
	
	
	public  void clearHandledStatus() {
		optionsHandledHash = new Hashtable();
	}

	public  void removeSubscriberProxy(String opt) {
		if (optionsSubscriberHash.containsKey(opt)) {
			optionsSubscriberHash.remove(opt);
		}
		if (optionsHandledHash.containsKey(opt))
			optionsHandledHash.remove(opt);
	}
	
	
}
